﻿*************************************************************************************************************************************
ReadMe file accompanying data file for: 
"Logging, exotic plant invasions, and native plant reassembly in a lowland tropical rainforest."

Authors: Timm F. Döbert, Bruce L. Webber, John B. Sugau, Katharine J.M. Dickinson & Raphael K. Didham

Published in: Biotropica
DOI: 
*************************************************************************************************************************************
File name: PlotData.csv
Description: This csv file contains the full database of characteristics for each of 159 vegetation plots (A-F and LF) including logging metrics, environmental variables as well as specific data on native and exotic plants.

Contact authors: Timm Döbert (timm.dobert@gmail.com); Raphael Didham (raphael.didham@csiro.au)

Column headings:
plot.code: Vegetation plot unique identifier
block: One of seven sampling blocks (A-F and LF) across the study landscape 
fragment: One of three fragment designations (1ha, 10ha or 100ha) within a block
north: UTM northing coordinate
east: UTM easting coordinate 
elevation: The elevation of a plot (m) 
forestloss17: The proportion of forest canopy loss at the local scale, i.e. within a 17m radius (%)
forestloss562: The proportion of forest canopy loss at the landscape scale, i.e. within a 562m radius (%) 
roaddensprim: Regional-scale density of continuously-maintained primary logging roads (km/km2)
roaddenssec: Regional-scale density of occasionally-used secondary logging roads (km/km2)
roaddistprim: Distance to nearest primary road (m)
roaddistsec: Distance to nearest secondary road (m)
river: Distance to nearest river (> 3m width; m) 
pH: Soil pH
humus: Soil humus depth (cm) 
soilN: Total soil nitrogen content (mg.cm-3)
soilP: Plant available soil phosphorus content (µg.cm-3)
bulkdens: Soil bulk density (g.cm-3)
soilmoist: Soil moisture factor
soilPC1: Principal component axis 1 scores from a PCA of the six soil-biogeochemical variables 
soilPC2: Principal component axis 2 scores from a PCA of the six soil-biogeochemical variables 
ex.richness: The species richness of exotic plants.
ex.drywght: The dry above-ground biomass of exotic plants.
ex.lai: The leaf area index of exotic plants.
native.richness: The species richness of native plants.
native.drywght: The dry above-ground biomass of native plants.
native.lai: The leaf area index of native plants.
dissim: dissimilarity of each of the logged forest plots (blocks A–F and LF) to the average dissimilarity of the 33 old-growth (OG) plots as a collective old-growth forest reference point derived from the Gower distance matrix in the NMDS analysis
clihir.pre: The presence (1) or absence (0) of Clidemia hirta.
clihir.abs: The absence of Clidemia hirta (1). NOTE: This column is required for the GLMM analysis.
dipt.drywght: The dry above-ground biomass of dipterocarp trees.
native.drywght.nodipt: The dry above-ground biomass of native plants excluding dipterocarps.
************************************************************************************************************************************
File name: SpeciesData.csv 
Description: This csv file contains the complete list of species (723) sampled across 192 vegetation plots (2 x 2m; A-F, LF and OG) located at the Stability of Altered Forest Ecosystems (SAFE) project in Sabah, Malaysia, including a distinction between native and exotic species origin.

Contact authors: Timm Döbert (timm.döbert@gmail.com); Raphael Didham (raphael.didham@csiro.au)

Column headings:
species.code: Unique code for each plant taxa
family: Plant family name
genus: Plant genus name 
species: Plant species name or unique identifier where species indeterminate
species.name: The genus and species name
origin: Distinction between native (n) and exotic (e) plant species
***********************************************************************************************************************************
File name: PlotSpeciesData.csv
Description: This csv file contains a matrix of biomass values for 723 plant taxa sampled across 192 vegetation plots (2 x 2m; A-F, LF and OG) located at the Stability of Altered Forest Ecosystems (SAFE) project in Sabah, Malaysia.

Contact authors: Timm Döbert (timm.dobert@gmail.com); Raphael Didham (raphael.didham@csiro.au)

Column and row headings:
Columns: The unique identifiers of 723 plant taxa
Rows: The 192 vegetation plots across 3 fragments nested within 8 blocks 
Matrix values: The dry weight biomass (g/m2) of each species in each plot
***********************************************************************************************************************************
