# Suivi du projet S8

***Semaine 1***
- Rendez-vous avec Mr Tatoni et Mme Dumas afin de prendre connaissance du sujet
- Reception du premier jeu de données (très léger pour faire du machine learning)


***Semaine 2***
- recherche de cours de machine learning
- installation des outils (notebook + jupyter)
- Envoi de mails avec Mme Dumas au sujet du jeu de données


***Semaine 3***
- Demande de prise de rendez vous avec Madame Dumas
- compréhension des jeux de données envoyés
=> pistes de début de projet
 - mettre les données au format csv pour pouvoir les traiter sur jupyter
 - voir comment récupérer les données météorologiques des différents emplacements et comment les mettre en corrélation
   avec les données que l'on a deja à notre disposition


***Semaine 4***
- Etablir le cahier des charges
- définir des tâches à faire en plus de l'apprentissage du ML et des techniques d'IA

***Semaine 5***
Formatage des données + création de notebook pour convertir les données

***Semaine 6***
- Recherche de projet similaire (avec plus de données) pour montrer de quel façon on peut se servir du machine learning dans l'observation du vivant
- Nettoyage des données


***Semaine 7***
- Présentation intermédiaires
- Recherche de sujet similaire pour faire du machine learning et de la visualisation de donnée

***Semaine 8***
- Poursuite de recherche de jeu de données puis selection entre nous des données que nous pourrions traitées
- Analyse des données choisies

Source des donnée que nous avons choisi : https://datadryad.org/stash/dataset/doi:10.5061/dryad.rb237

***Semaine 9***
- Recherche d'algorithmes de machine learning
- Visualisation de donnée sur l'ancien jeu de données (REEVES)
- recherche d'API de conversion de coordonnées (UTM -> long lat)
  -> pour la localisation des données UTM, le code UTM de la region géographique des données est `50N`
  -> geocodio : api key = `2626308063f4f64b688c8f08bf7783b24f7d6fb`
- réflexion du (ou des) problèmes que l'on cherchera à résoudre lors que les données seront exploitables
  => observer l'évolution des espèces invasives en fonction des paramètres de la biomass.

***Semaine 10***
- Etude de la corrélation entre les différentes métriques sur le jeu de données REEVES + visualisation de ces données.

- compréhension du jeu de données choisi la semaine précédente
- réflexion sur le nettoyage des données
 -> réfléxion sur la façon dont on va se traiter des valeurs nulle

- recherche d'API de conversion de coordonnées (UTM -> long lat)
  -> pour la localisation des données UTM, le code UTM de la region geographique des données est `50N`
  -> geocodio : api key = `2626308063f4f64b688c8f08bf7783b24f7d6fb`

- Ecriture d'un script pour convertir les données coordonnées.

- réflexion du (ou des) problèmes que l'on cherchera à résoudre lors que les données seront exploitables
  => observer les l'évolution des espèces invasives en fonction des paramètres de la biomass.

***Semaine 10***
Revu de projet : Aucune des données récoltées permettent de faire une analyse de la dynamque de la végétation car elles ne prennent pas en compte l'aspect temporelle

=> Nous avons décidé de reprendre le jeu de données REEVES pour mettre en place un algorithme d'apprentissage dessus bien que nous savions déjà que le resultat sera faux.

- Rendez-vous avec un professeur de machine learning (Mme Charbonnier) pour nous conseiller un dans cette nouvelle réflexion.

***Semaine 11***
- essayer de faire tourner un algorithme de machine learning
  - finir de préparer les données
  - ecrire le code pour le faire tourné

- préparer la soutenance
  - faire un plan
  - expliquer le dérouler du projet, les obstacles rencontrés et les décisions prises
  - expliquer pourquoi les resultats ne sont pas satisfaisant et comment mettre en place un protocole de récupération de données qui pourrait donnée des resultats satisfaisant
  - parler des données alternatives qui nous ont permit d'essayer de faire sur la visualisation des données


  # Rapport

  ## Introduction
  Dans le cadre de notre projet, nous nous sommes intéressé à l'évolution d'espèces invasives le long de voie férrées.
  En effet, depuis quelques années des espèces exotiques se sont développées le long des voies férrées compliquant le passage des trains le long des ces dernières.

  Dans le cadre du projet REEVES (Recherche sur les Espèces Exotiques Végétales EnvahissanteS), des chercheurs ont mis en places des protocoles pour essayé de diminuer le developpement de ces plantes et cherchent à mesurer leur efficacité. Nous nous intéresserons au cas du Mimosa, de l'Ailante et de la Canne.

  Le protocole consiste perturbé le developpement de la plante invasive en ajoutant dans son environnement des espèces alélopathiques consommant les ressources du sol.
  Les plantes sont placé sur différentes placettes. Sur certaines d'entre elles, aucun traitement n'a été mis en place (ces placettes servent de référence), sur les autres placettes différents traitements ont été mis en place, ceux pour lesquels on veut mesurer l'efficacité.

  Des mesures ont été effectuées sur 3 sites :
  - Saint Raphaël
  - Morière
  - Joncquerette

  Ne parvenant pas a obtenir des resultats avec les outils d'analyse classique, les chercheurs nous ont donc contacter afin de répondre à la problématique suivante :
  - Comment le machine learning peut apporter de nouvelles options à l'observation du vivant
  - Que peut-on dire de la trajectoire (ou dynamique) en fonction des traitement au cours des années 2021 et 2022 ?



  ***Notre travail***
  - Prise de connaissance des données
  - récupération de plus de données car les premières étaient insuffisantes
  - recherche sur le machine learning
  - tentatives de mise en forme des nouvelles données mais ces dernières contenaient trop de trou.
      + mise en forme des données
      + conversion au format csv
          (les scripts de conversion se trouvent dans le répertoire REEVES/data)
          (les données mise au format csv se situent dans le répertoire REEVES/data/data_csv)

  - recherche de nouveaux jeu de données

  - compréhension du jeu de données choisi
  - visualisation des ces données à l'aide de kepler (en parallèle)
      + convertion de certaines de ces données afin de les rendre visualisable sur kepler
          les scripts de conversions se situent dans
      + debut d'analyse de ces données

  => Les données ne nous permettaient pas de mettre en évidence les objectifs demandés car elles ne prennaient pas en compte l'aspect temporel

  - Mise en place d'un modèle de machine learning avec le jeu de données de départ que deviendrai meilleurs avec une récupération des données plus fréquente.
  => Bien que nous puissions mettre en place un modèle, n'ayant que 3 mesures et trop de valeurs nulles aucun outils du machine learning ne peut conclure quoi que ce soit de fiable

  ***Pour que cela fonctionne***
  Prendre des mesures à intervalle de temps plus régulier, cela permettra d'avoir plus de données mais également une analyse plus fiable. En effet les metriques nous permettant de mesurer l'efficacité d'un traitement étant la variation du nombre de feuille,la variation de la hauteur moyenne des plantes ainsi que la variation du nombre de plantes. Il nous faudrait donc plus de mesures à intervalle de temps plus régulier pour pouvoir mesurer une dynamique précise concernant l'évolution des espèces invasives en fonction de chaque traitement
