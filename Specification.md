1 - Context and definition of the problem

Within the framework of an ecology project, we are interested in the evolution of invasive species over the last few years on different areas. Indeed, these invasive species are at the origin of a supply of biodiversity (impoverishment of the grounds not allowing other species to develop). These species are located near railroad tracks, it hinders the movement of trains trying to run on them.
The costs generated to get rid of these species are high. Researchers are therefore interested in methods to get rid of them. 
To do this, they use allopathic species that contribute to slowing down the evolution of these invasive species. 
Based on the data they have collected over the past two years, they are looking to use tools such as machine learning to understand what treatment is most effective in dealing with these invasive species.



2 - Objective of the project

The objectives of this project will be the following:

To highlight the parameters that slow down the evolution of these invasive species and to bring an explanation on the dynamics of the vegetation from different protocols and measurements on 2 dates. 
To bring new useful information, thanks to machine learning algorithms, which could not be obtained from previous statistical studies.
To link geographical data corresponding to the measurements taken in order to observe if location can influence the evolution of these species.
Use data visualization tools to highlight what we conclude from our analyses.



3 - Scope


In order to ensure the progress and success of our project, we must set time and objective constraints. 
We have therefore decided to apply only machine learning algorithms on the data provided to us, which we will have previously formatted. 
We will also be able to use the data provided by the openweather map API. 
This scope may change over time, depending on your expectations and our results.


4 - Steps of the project

Produce from the data provided, a relevant dataset for machine learning (data formatting).
Choice of machine learning algorithm(s).
Analyze the data using the chosen algorithms. 
Visualize the data and analyze it by including mappings.


5 - Deadlines

Until 27/02 -> 3h/week (6x 3h = 27h)
Until 27/03 -> 6h/week (4x 6h = 24h)
27/03 and 28/03 -> 9h (1x9h = 9h)

Total : 60h of project

=> Realization of the planning in progress


