# Rapport de synthèse

## Introduction

### Présentation

Dans le cadre de notre projet, nous avons été contacté par des chercheurs de l'IMBE, Thierry Tatoni et Estelle Dumas s'intéressant à l'évolution d'espèces envahissantes végétales le long de voies ferrées dans le cadre du projet REEVES.

#### IMBE

L’Institut Méditerranéen de Biodiversité et d’Ecologie marine et continentale (IMBE) développe une approche intégrative pour l’étude de la biodiversité et des systèmes socio-écologiques.
L’IMBE apporte ainsi des connaissances fondamentales et appliquées sur les fonctions, la dynamique historique et évolutive de la biodiversité de tous types d’écosystèmes méditerranéens, depuis la construction des paléo-écosystèmes jusqu’à leur devenir dans le contexte du changement global. Ces apports incluent également les liens avec la société civile d’une part et les enjeux de santé humaine d’autre part.
A travers son implication dans la recherche, la formation et la valorisation scientifique, l’IMBE participe activement à la transition environnementale et au développement durable pour la définition de politiques environnementales tant locales que nationales et internationales.
(site de l'IMBE)

#### Projet REEVES

Le projet REEVES est un projet de la SNCF dont l'acronyme signifie `Recherche sur les Espèces Exotiques Végétales EnvahissanteS`.

Le projet de recherche rassemble de nombreux partenaires avec 15 chercheurs, la Région Grand-Est et la Région Sud. Ainsi que plusieurs directions comme les Zones de Production, les Directions Territoriales régionales et la Direction du Développement Durable de SNCF.
Sur la base d’un budget de 2 millions d’euros sur les années 2019 et 2023, le projet associe la recherche en laboratoire et des stations expérimentales sur les talus ferroviaires.

***Espèces exotiques envahissantes***

"Une espèce exotique envahissante est une espèce allochtone dont l'introduction par l'Homme (volontaire ou fortuite), l'implantation et la propagation menacent les écosystèmes, les habitats ou les espèces indigènes avec des conséquences écologiques, économiques ou sanitaires négatives." (UICN, 2000).

Dans le cadre du projet REEVES (Recherche sur les Espèces Exotiques Végétales EnvahissanteS), des chercheurs de l'IMBE ont mis en places des protocoles pour essayer de diminuer le développement de ces plantes, et cherchent à mesurer l'efficacité de ces derniers. Ils se sont intéressés au cas du Mimosa, de l'Ailante et de la Canne. Pour cela, ils cherchent à résoudre ce problème par la nature, c'est à dire avec des solutions naturelles.

### Protocoles expérimentaux

Le protocole expérimental mit en place consiste à perturber le développement de la plante exotique envahissante en ajoutant dans son environnement des espèces allélopathiques consommants les ressources du sol. Ils ont réalisé cette expérience sur 3 sites, nommés respectivement Saint Raphael, Morière et Joncquerette.

Pour cela, ils ont mis en place des placettes de 4m sur 4m, espacées de 50cm, sur lesquelles se situe une espèce envahissante. Différentes plantes (allélopathiques ou ajout de compost) ont été ajouté sur ces placettes comme sur la figure ci-dessous dans le but de mesurer leur impact sur l'évolution de l'espèce envahissante. Une de ces placette ne contenait aucune autres végétation dans son environnement, afin de servir de placette témoin.

![](images/experience.png)

### Problématique

Ne parvenant pas a obtenir des résultats avec les outils d'analyse classique, les chercheurs nous ont donc contacté afin de répondre à la problématique suivante :

- Que pourrait apporter le machine learning à l'observation du vivant
- Que peut-on dire de la trajectoire (ou dynamique) en fonction des traitements au cours des années 2021 et 2022 ?

### Resumé de notre démarche

Dans un premier temps, nous avons pris contact avec eux afin qu'ils nous expliquent leur projet ainsi que ce qu'ils attendaient de nous.
Nous avons ensuite récupéré un premier jeu de données que nous avons dans un premier temps, essayé de comprendre. Ce jeu de données étant trop petit, nous avons alors demandé d'autres jeux qui paraissaient plus exploitables pour faire du machine learning.

Nous avons alors tenté de récupérer des données supplémentaires, telle que la météo (grâce à l'API openweathermap). Malheureusement, nous ne pouvions récupérer que les données météo en temps réel. Nous avons donc été contraints d'oublier cette piste.
En parallèle nous avons cherché à mettre en forme les données pour les rendre exploitables par un algorithme de machine learning. C'est à cette étape que nous avons constaté qu'il y avait trop de valeur nulles parmi les données et que nous ne pourrions pas avoir de résultats satisfaisants en faisant tourner des algorithmes de machines learning dessus.

Nous avons donc cherché des jeux de données existants pour essayer d'apporter une réponse à notre problématique. Il était difficile de trouver un projet similaire mais nous avons trouvé un jeu de données qui s'y apparentait. Nous parlerons de ce jeu de données plus loin.

Ensuite, nous avons étudié ce jeu de données, pour montrer en quoi l'outil Kepler pouvait être utile afin de visualiser des données géographiques.
C'est en cherchant à trouver des objectifs à analyser en rapport avec ce jeu de données que nous avons décidé de faire machine arrière. En effet, ces données de comprenaient pas de données temporelles et ne permettaient donc pas de répondre à la problématique qui nous était proposée.

Ne trouvant pas de jeu de données respectant les contraintes, nous avons alors décider de repartir sur le jeu de données REEVES.

Nous avons alors à nouveau traité les données pour créer des modèles de machine learning se basant sur des arbres de décision bien que nous savions que les résultats ne pourraient pas être satisfaisants au vu du manque de données entrées.

## 5. Gestion de projet : Gantt

  Pour mener à bien la résolution du problème posé dans les délais, nous décidons d’adopter **la méthode de développement agile**. Ainsi le déroulement de notre travail a été marqué par :

- **L’implication du client dans le processus :** pour fournir les besoins, établir le cahier des charges;
- **La livraison incrémentale :** les notebooks Jupiter permettent d'avoir un livrable fonctionnel à chaque étape du développement;
- **L’adaptation aux changements :** en effet, nous avons vite constaté que l'étude dynamique demandée par le client n'était pas réalisable au vu de la faible quantité de données fournie et nous avons proposé des alternatives;
- **La simplicité :** avec une réduction des charges liées à la documentation car les notebooks contiennent à la fois le code et de markdown.

Ainsi donc, avec une équipe de projet constituée de 4 personnes et des changements dans les besoins pouvant être apportés à tout moment du cycle de développement du projet, la méthode agile s’avère être appropriée, car flexible et nous permet de fournir des livrables après chaque étape.

**Diagramme de Gantt**
![](images/Gantt.png)

### **6- Explication des jeux de données brutes**

Le dataset avec lequel nous avons travaillé est constitué de 3 fichiers excel qui sont décrits ci-dessous :

- **rel_synth.xlsx** : présente la proportion d'occupation de chacune des espèces présentes dans les différents sites. Ce classeur excel est organisé selon une EEE (Espèce Exotique Envahissante) par feuille, par site pour 2 dates (2022 et 2021).

- **Rejetinvasivesbilan2021-2022.xlsx** : un second jeu de données sur la croissance des plantes invasives pour les différents traitement et les 3 sites. ce fichier contient les métriques (Hauteur, Nb feuilles, Nb rameaux, Nombre, Nombre de Noeuds)
En raison d’autocorrelation spatiale, les relevés des 3 quadrats de 1m2 ont été fusionnés en 1seul relevé. Donc l'ensemble des données de ce classeur sont résumée dans une seule feuille donc la description est la suivante :
![](images/null_Resume.png)

###### <div align="center">Description du dataset sur l'évolution EEE</div>

=======

- **Rejetinvasivesbilan2021-2022.xlsx** : un second jeu de données sur la croissance des plantes invasives pour les différents traitements et les 3 sites. Ce fichier contient les métriques (Hauteur, Nb feuilles, Nb rameaux, Nombre de plantes, Nombre de Noeuds)

>>>>>>> 74f4181dfd399f2f7d4f1f04e478061b1ca0c385

- **Mesure Plantes Allélopathiques 2022.xlsx** : contient la mesure des plantes allélopathiques.

## Travail Réalisé

### 1. Traitement de données

#### Jeu de données REEVES

Dans le but de créer des algorithmes de machine learning, nous avons du préparer les données.

Les données que nous avions à notre disposition étaient des données sous le format de documents Excel, nous avons donc chercher à les transformer en données au format CSV.

Au vu du manque d'informations dans certains jeux de données, nous avons décidé de ne garder que les jeux de données qui possèdent le plus d'informations. Nous avons donc décidé de résumer toutes les feuilles du jeu de données data.xlsx dans le fichier dataset.xlsx. Grâce à cette mise en forme de données, nous nous sommes rendus compte qu'il nous manquait beaucoup d'informations (cf les cases rouges). Nous nous sommes demandés si nous ne pouvions pas simuler les données manquantes en les encadrant entre deux valeurs connues par exemple. Malheureusement, comme nous n'avions que 2 relevés (un en 2021 et un en 2022), nous n'avons pas pu prédire par encadrement les données manquantes. Nous avons donc conclu que le fichier data.xlsx ne pourrait pas être utilisé dans des modèles de machine learning car les résultats seraient complètement erronés au vu du manque de données présent dans ce dernier.

Nous avons donc décidé de nous focaliser sur le fichier Rejetinvasivesbilan2021-2022.xlsx car c'est celui qui était le plus à même d'être utilisé. Une fois les données de ce fichier converties au format CSV, pour les intégrer dans un algorithme d'arbre de décision, nous avons du trouver un moyen d'exprimer les différents traitements en fonction de valeurs binaires. Pour chaque mesure, nous avons créé une colonne indiquant 1 si le traitement correspondant à la colonne est présent sur cette mesure et 0 sinon.

Nous avons également calculé la variation des différentes mesures effectuées pour chaque placette au cours du temps en les identifiants en fonction de leur traitement et de leur site.

### 2. Visualisation de données(*Durel*)

L'objectif de cette partie est d'effectuer la visualisation de données notamment pour essayer de ressortir des patterns dans nos données et l'évolution de l'évolution des EEE. Cette visualisation a été réalisée à l'aide des bibliothèques python : Matplotlib et Seaborn.

N.B : Dans ce chapitre nous présenterons uniquement les résultats les plus pertinent obtenus à partir des données contenues dans le fichier Rejetinvasivesbilan2021-2022. Cependant, nous avos fourni d'autres notebooks contenant d'autres graphiques moins pertinents.

**Corrélation entre les données**
![](images/Correlation_res.png)

**Evolution de la hauteur moyenne des EEE par traitement**

![](images/Hauteur.png)
![](images/Evolution_hauteur.png)

**Interprétation : évolution de la hauteur**
On constate que la hauteur moyenne est de 160 cm pour les parcelles témoins et 175cm pour les invasives.
Les graphiques ci-dessus nous montrent que toutes les traitements avec espèce allélopatique freine l'évolution des EEE en terme de hauteur. Mais ce sont les traitement **NA1 avec compost, et NA2** qui sont les plus efficaces avec un ralentissement de plus de **50%**.

### Evolution du nombre totale des feuilles par traitement

![](images/total_nbfeuille.png)
![](images/evolution_nbf.png)

#### Interprétation

Tout d'abord les mésures éffectuée en novembre 2021 ne peuvent pas être considérées très significatives en terme d'efficacité du traitement car il s'agit d'une période hivernale et les plantes dépérissent d'elle même en hivers.

Cependant, ces graphiques nous montrent une grande dimunition de la densité foliaire (nombre de feuilles) des EEE pour certains traitement, Ainsi :

- pour les traitements **NA1 avec compost**, **Compost unique** et **NA2**, on a une diminution de la densité foliaire des EEE d'environ 50%
- Par contre la baisse est moins notable pour les traitements **NA2 avec compost**, **Compost unique** et **NA1**,

### 3. Modèle de machine learning(*Durel*)

#### 3.1. Description des algorithmes utilisés

#### &nbsp;&nbsp;&nbsp;&nbsp; a. ACP

  L' analyse en composantes principales (ACP) est un algorithme d'apprentissage non-supervisé qui permet de réduire la dimension (Nombre de variables) des données sans grande perte d'information et ainsi faciliter leur visualisation. En outre, l'ACP permet aussi d'augmenter la performance des algorithmes de machine learning car moins efficace sur des données à grande dimension.

  L'idée est de transformer des variables corrélées en nouvelles variables décorrélées en projetant les données dans le sens de la variance croissante. Les variables avec la variance maximale seront choisies comme les composants principaux.

#### &nbsp;&nbsp;&nbsp;&nbsp; b. Kmeans

L'algorithme Kmeans est un algorithme itératif qui tente de partitionner un ensemble de données en K sous-groupes ou clusters distincts, où chaque point de données appartient à un seul groupe.
Plus généralement l’algorithme des k − moyennes obéit aux étapes suivantes :

1. Choisir k points, par un tirage aléatoire sans remise et les considérer comme des centroïdes
2. Distribuer les points dans les k classes ainsi formées selon leur proximité aux centroïdes
3. Utiliser les barycentres des classes comme nouveaux centroïdes et répéter jusqu’à ce qu’il n’y ait plus de changement.

Remarque : Cette algorithme présente une forte dépendance au choix du nombre k de classes

***Méthode du coude***

La méthode du coude nous permet de choisir un bon nombre k de clusters. Elle se base sur la somme de la distance au carré (SSE) entre les points de données et les centroïdes de leurs clusters prédit par l'algorithme. Il suffit donc de tracer les SSE en fonction des K et nous sélectionnons k à l'endroit où SSE commence à s'aplatir et à former un coude.

#### &nbsp;&nbsp;&nbsp;&nbsp; c. Decision Tree

Approche en mode "Si ... Alors ...". L'objectif est de diviser les valeurs que l'on possède, à partir de plusieurs paramètres. Ainsi, on est capable de résoudre des problèmes de régression ou de classification.
L'idée de base est de diviser les données en sous-groupes de plus en plus petits en fonction de caractéristiques spécifiques jusqu'à ce que chaque sous-groupe ne puisse plus être divisé.
Pour séparer nos données, il faut réfléchir à comment on le fait, et comment on choisit les valeurs de décision :
L'idée est de trouver la variable la plus importante pour classifier nos données. Cela se fait en calculant l'importance de chaque variable. Plus la variable est importante, plus elle est utilisée tôt dans l'arbre.

- cherche la variable la plus importante pour la classification des données.
- ensuite, l'algorithme va diviser les données selon le critère le plus important, pour en faire 2 sous-groupes.
- on reproduit ensuite par récurrence ces étapes, jusqu'à ce que toutes nos données soient classifiées de manière homogène.

#### &nbsp;&nbsp;&nbsp;&nbsp; d. Random forest

Le principe de la forêt aléatoire est simple, il s’agit de multiplier les arbres. C'est un algorithme qui se base sur l’assemblage d’arbres de décision.
Un random forest est constitué d’un ensemble d’arbres de décision indépendants.
Chaque arbre dispose d’une vision parcellaire du problème du fait d’un double tirage aléatoire : un tirage aléatoire avec remplacement sur les observations et un tirage aléatoire sur les variables
A la fin, tous ces arbres de décisions indépendants sont assemblés. Et l'estimation finale consiste à choisir la catégorie de réponse la plus fréquente.

#### **3.2. Implémentation et interprétation des résultats**

Du fait de la trop faible fréquence de mesure et des nombreux vides contenus dans notre dataset, il est impossible d'envisager une tache de régression sur nos données. Nous avons donc décidé de nous orienter vers la classification des traitements qui ont été effectués sur le EEE afin d'essayer de les classer du moins bon au plus efficace.

Afin d'appliquer les arbres de décisions, nous devons séparer nos données en entrées et sorties.

En entrée, nous prenons les caractéristiques du traitement appliquées :
$ X =( [\text {NA1, NA1+Sol, NA2, NA2+Sol, Sol, Temoin, Invasive} ]*[\text{avec compost, sans compost}])^n$ avec n le nombre de lignes d'enregistrement

et en sortie l'évolution de l'EEE (variation du nombre, de la hauteur et du nombre de feuilles).

$ Y =( \text {var. Nombre, var. Hauteur, var. Nb Feuilles})$

l'état actuel de nos données ne nous permet pas de les exploiter directement avec un arbre de décision, donc nous allons suivre les étapes ci-dessous :

- **Mise en forme des entrées** :

 Tout d'abord nous allons transformer nos entrées en entrées binaires tel que ; $ X =( \text {NA1,NA2, Sol, Temoin, Invasive, Compost} ) \text{avec NA1, NA2, Sol, Temoin, Invasive, compost }\epsilon [0,1]^n $(1 si le traitement à la ligne contient le traitement correspondant et 0 sinon )

Exemple : Dans la capture ci-dessous, le traitement correspondant à la ligne 0 est (NA1, sans compost) et celui correspondant à la ligne 6 est (NA2, sans compost)

![](images/exemple.png)
Si on a des 1 à la fois sur NA1, Sol et compost cela correspond au traitement (NA1+Sol, Avec compost).

- **Transformation de la sortie** :

Ici, l'objectif est de réunir les trois variables de sortie en une seule variable qui va représenter la monotonie de l'evolution de l'EEE ( Par exemple 3 groupes : Croissance(C), Stable(S), Diminution(D)).

Les différents graphiques réalisés ne font ressortir aucun pattern sur les données. Nous allons donc réaliser une clusterisation de nos données à l'aide de l'algorithme de Kmeans.

Tout d'abord, suite à une étude de la corrélation entre nos variables de sortie, on remarque des corrélations un peu élevées, ce qui motive notre choix de réaliser un ACP sur ses données afin d'augmenter les performances de notre algorithme de k-means.

![](images/Correlation_sortie.png)

- PCA :
Après avoir normalisé nos données, on applique l'ACP pour obtenir leurs projections sur les axes principaux

![](images/ACP.png)

On remarque que la variance expliquée des deux premières composantes PC1 et PC2 représente plus de **80%** de la variance expliquée totale. Donc le PC1 et PC2 contiennent plus de 80% de l'information contenu dans toutes nos variables de sortie; c'est avec elles que l'on va diviser nos données en cluster.

- K-means :

La première étape est de déterminer la valeur optimale de K (Nombre de centroïdes). Nous utilisons **la méthode du coude** comme suite :

![](images/coude.png)

On observe un coude pour **K=3**

On effectue donc notre clusterisation avec k=3.

![](images/Kmeans.png)

###### <div align="center">Algorithme de K-means</div>

Le graphique ci-dessous représente les différents clusters.

![](images/kmeans_graphic.png)

###### <div align="center">Cluster des données de sortie</div>

<div align="justify">Texte ou image</div>

Ce résultat coincide avec l'hypothèse de 3 monotonies dans évolution des EEE. Cependant, à première vue, on ne connaît pas à quoi correspond chacun des clusters. Nous allons donc interpoler nos clusters avec nos données initiales.

![](images/data_cluster.png)

Ensuite, nous traçons les nuages de points de nos variables de sortie en fonction de nos clusters :
![](images/graphe_cluster.png)

#### Interprétation

- classe 0 : correspond à une stabilisation de l'évolution car le nuage de point est centré en 0
- la classe 1 : Augmentation des EEE, la hauteur et le nombre de feuilles sont les plus importants
- la classe 2 : Diminution des EEE, Nf < 0 et hauteur stable.

### Decision Tree

Nous allons maintenant appliquer l'arbre de décision sur nos données, avec en entrée, :  $X =( \text {NA1,NA2, Sol, Temoin, Invasive, Compost} ) \text{avec NA1, NA2, Sol, Temoin, Invasive, compost }\epsilon [0,1]^n $
et en sortie : $Y =(cluster)$

![](images/decision_tree.png)

###### <div align="center">Decision Tree</div>

On obtient donc un score de précision de **60,5%**

On fait ensuite des prédictions sur nos traitements :
![](images/Prediction.png)
![](images/result_pred.png)

### 4. Second jeu de données

Durant notre projet, nous nous sommes tournés vers d'autres jeux de données car ceux qui nous ont été fournis manquaient d'informations pour pouvoir exploiter des résultats fiables. Nous avons décidé de regarder un jeu de données alternatif sur l'impact des plantes exotiques sur les plantes indigènes dans des exploitations forestières et vise-versa. L'idée était de voir, à partir de ce jeu de données, si l'exploitation des forêts avait un impact sur l'invasion de plantes envahissantes et si la combinaison de ses plantes invasives avaient un impact sur les plantes locales. Pour ce faire nous avions accès à 3 jeux de données :

- PlotDataBiotropica.csv, qui contient la base de données complète des caractéristiques de chacune des 159 placettes de végétation, y compris les mesures d'exploitation forestière, les variables environnementales ainsi que des données spécifiques sur les plantes indigènes et exotiques.
- SpeciesDataBiotropica.csv, qui contient la liste complète des espèces (723) échantillonnées dans 192 parcelles de végétation (2 x 2 m) situées dans le cadre du projet SAFE (Stability of Altered Forest Ecosystems) à Sabah, en Malaisie, y compris une distinction entre l'origine des espèces indigènes et exotiques.
- PlotSpeciesDataBiotropica, qui contient une matrice de valeurs de biomasse pour 723 taxons végétaux échantillonnés dans 192 placettes de végétation (2 x 2 m) situées dans le cadre du projet Stability of Altered Forest Ecosystems (SAFE) à Sabah, en Malaisie.

Le jeu de données que nous avons récupéré était complet, nous avons donc essayé de faire de la visualisation de données à l'aide de Kepler. Le seul soucis était que les coordonnées étaient au format UTM qui n'est pas un format exploitable sous Kepler, nous avons donc utilisé une API nous permettant de convertir les coordonnées en coordonnées de type longitude/latitude puis ajouter les coordonnées obtenues dans le jeu de données.
Le fichier de conversion de ces données est disponible avec le path (alternatives_data/UTM_to_lon_lat.ipynb)
>>>>>>> 74f4181dfd399f2f7d4f1f04e478061b1ca0c385

***Visualisation de données avec Kepler***

L'outils Kepler nous permet de voir où les placettes du jeu de données se situent géographiquement. On peut également ajouter un contraste de couleur sur chacun des points affichés pour avoir un rapide aperçu de la variation de la mesure que l'on regarde d'un point à un autre.
un exemple de visualisation est disponible dans le notebook (/alternatives_data/visualisation_geographique.ipynb)

## Problèmes rencontrés

Au cours de ce projet, nous avons rencontré des difficultés qui nous ont obligé à faire de la revue de projet à plusieurs reprises.

Dans un premier temps, il y avait beaucoup de valeurs absentes dans le jeu de données que nous avons récupéré et il n'était pas possible d'essayer de les retrouver car trop peu de mesures ont été effectuées.

Le manque de données nous empêchant de réaliser un algorithme de machine learning correct nous a mené à la recherche d'un nouveau jeu de données.

Après une semaine de recherche, nous avons dû retenir un nouveau jeu de données sur un projet similaire. Cependant, nous n'avons pas pu retenir ce jeu de données pour faire du machine learning dessus car c'était un jeu de données statique, ce qui allait à l'encontre de la problématique du sujet. Nous avons cependant pu faire de la visualisation géographique dessus.

Finalement, nous avons décidé de retourner sur le jeu de données initial bien que nous savions qu'il ne nous permettrait pas d'avoir des résultats exploitables.

## Proposition d'amélioration

  On constate que la  précision de notre arbre de décision est au maximum de 60%, ce n'est pas suffisant pour identifier clairement un traitement comme étant efficace. Pour améliorer les chances de réussir on pourrait :
  
- Prendre des mesures à interval de temps plus régulier, ce qui permettrait d'avoir jeu de données plus important. ça nous permettrait également d'appliquer des modèles de regression pour prédire l'évolution future.

- Au niveau des types de données récoltées, nous avons utilisé les données correspondantes aux nombres de plantes invasives, de leur hauteur et de leur nombre de feuilles.
Un des jeux de données contient des informations concernant la proportion qu'occupent les espèces envahissantes sur une parcelle. Il serait possible de mettre en relation les jeux de données pour avoir des informations suplémentaires.
pour mettre en relation les jeux de données plus facilement
on peut Donner des identifiants aux parcelles, de cette façon si on fait des mesures différentes sur l'une d'entre elle, il est possible des les identifier dans tous les jeux de données pour les mettre en relation.

- Afin d'identifier le traitement qui sera le plus efficace, il faudra pouvoir définir sur quels critères un traitement est considéré comme efficace sur l'espèce invasive (-> Par exemple, si sa proportion diminue de plus de 50%, alors on peut peut être considérer le traitement comme étant efficace).


- Il peut aussi être intéressant de prendre en compte la météo, l'ensoileillement, l'humidité du sol lors de la prise des mesures. Lors de nos recherches, nous avons pu voir qu'il était possible de consulter les données météorologiques, mais que pour avoir un historique détaillé, il fallait un abonnement. Nous nous en sommes donc restés ici, mais il est cependant possible de coupler ces données aux précédentes pour pouvoir prendre en compte l'environnement. Dans le jeu de données alternatif que nous avions consulté, les données regroupaient des paramètres de pH du sol, ce qui peut aussi influer sur le comportement des espèces végétales.

  
## Conclusion

Ce projet nous a permis d'appréhender le travail de data analyst. Tout d'abord nous avons pu prendre connaissance d'un projet intéressant qui pourrait aider de manière conséquente l'entretien des bords des voies ferrées. Ce fut pour nous une réelle opportunité de pouvoir participer à ce projet concret, bien que nos résultats n'aient pas été très concluants.
Nous avons pu travailler sur le traitement de données qui fut une grande partie du travail. Nous avons aussi pu apprendre sur les notions de machine learning et expérimenter certains algorithmes. Nous avons aussi pu mettre en application nos compétences de gestion de projet tout du long de ce dernier, grâce à de nombreux rendez-vous pour faire des points sur notre avancement.
Tout ce projet nous a permis de nous rendre compte de l'importance des données et des possibilités offertes par le machine learning. Cependant, nous avons été déçus de ne pas avoir pu rendre des résultats fiables.
Nous espérons qu'au cours des prochaines années, les données seront plus étoffées ce qui permettra éventuellement de trouver un résultat satisfaisant.
