# Analysis and visualization of invasive species

##  1. Introduction

### a-Presentation

Within the context of our project, we were contacted by IMBE researchers, Thierry Tatoni and Estelle Dumas, who are interested in the evolution of invasive plant species along railroads within the context of the REEVES project.

#### IMBE

The Mediterranean Institute of Biodiversity and Marine and Continental Ecology (IMBE) develops an integrative approach to the study of biodiversity and socio-ecological systems.
The IMBE provides fundamental and applied knowledge on the functions, historical and evolutionary dynamics of biodiversity in all types of Mediterranean ecosystems, from the construction of paleo-ecosystems to their future in the context of global change. These contributions also include links with civil society on the one hand and human health issues on the other.
Through its involvement in research, training and scientific valorization, IMBE actively participates in the environmental transition and sustainable development for the definition of local, national and international environmental policies.
(IMBE website)

#### Project REEVES

The REEVES project is an SNCF project whose acronym stands for `Recherche sur les Espèces Exotiques Végétales Envahissantes`.

The research project brings together many partners with 15 researchers, the Grand-Est Region and the Southern Region. As well as several departments such as the Production Zones, the Regional Territorial Departments and the Sustainable Development Department of SNCF.
Based on a budget of 2 million euros over the years 2019 and 2023, the project combines laboratory research and experimental stations on railway embankments.

***Invasive alien species***

"An invasive alien species is an allochthonous species whose introduction by humans (voluntary or incidental), establishment and spread threaten ecosystems, habitats or native species with negative ecological, economic or health consequences." (IUCN, 2000).

Within the context of the REEVES project, researchers at IMBE have set up protocols to try to reduce the development of these plants, and are seeking to measure the effectiveness of these protocols. They are interested in the case of the Mimosa, the Ailante and the Cane. For that, they try to solve this problem by nature, that is to say with natural solutions.




### b-Experimental protocols

The experimental protocol set up consists in disrupting the development of the invasive exotic plant by adding in its environment allelopathic species consuming the soil resources. They carried out this experiment on 3 sites, named respectively Saint Raphael, Morière and Joncquerette.

To do this, they set up plots of 4m by 4m, spaced 50cm apart, on which an invasive species is located. Different plants (allelopathic or compost addition) were added on these plots as shown on the figure below, in order to measure their impact on the evolution of the invasive species. One of these plots did not contain any other vegetation in its environment, to serve as a control plot.


![](images/experience.png)

### c-Problem

Not being able to obtain results with classical analysis tools, the researchers contacted us in order to answer the following problem:

- What could machine learning bring to the observation of the living
- What can we say about the trajectory (or dynamics) according to the treatments in the years 2021 and 2022?

### d-Summary of our approach

First, we contacted them so that they could explain their project and what they expected from us.
Then we retrieved a first set of data that we tried to understand. This dataset being too small, we then asked for other datasets that seemed more exploitable for machine learning.

Then we tried to retrieve additional data, such as the weather (thanks to the openweathermap API). Unfortunately, we could only retrieve the weather data in real time. So we were forced to forget about this track.
In parallel, we tried to format the data to make them usable by a machine learning algorithm. It is at this step that we noticed that there were too many null values among the data and that we could not have satisfactory results by running machine learning algorithms on them.

So we looked for existing datasets to try to answer our problem. It was difficult to find a similar project but we found a dataset that was similar for us. We will talk about this dataset later.

Then, we studied this dataset, to show how the Kepler tool could be useful to visualize geographic data.
Trying to find objectives to analyze in relation to this dataset that we decided to go back. Indeed, these data did not include temporal data and thus did not allow us to answer the problematic that was proposed to us.

Not finding a dataset respecting the constraints, we then decided to start again on the REEVES dataset.

Finally, we processed the data again to create machine learning models based on decision trees although we knew that the results could not be satisfactory due to the lack of input data.

### e-Project management: Gantt

  In order to solve the problem in time, we decided to adopt the agile development method. Thus the course of our work was marked by :

- **The involvement of the customer in the process:** to provide the needs, establish the specifications;
- **Incremental delivery:** Jupiter notebooks allow us to have a functional deliverable at each step of the development;
- **Adaptation to changes:** Indeed, we quickly realized that the dynamic study requested by the customer was not feasible given the small amount of data provided and we proposed alternatives;
- **Simplicity:** with a reduction of the documentation charges because the notebooks contain both the code and markdown.

Thus, with a project team consisting of 4 people and changes in requirements that can be made at any time in the development cycle of the project, the agile method proves to be appropriate, as it is flexible and allows us to provide deliverables after each step.

**Gantt chart**


![](images/Gantt.png)

### f-Explanation of the original dataset 

The dataset we worked with consists of 3 excel files which are described below:

- **rel_synth.xlsx** : presents the proportion of occupation of each species present in the different sites. This excel workbook is organized according to an IAS (Invasive Alien Species) per sheet, per site for 2 dates (2022 and 2021).

- **Rejetinvasivesbilan2021-2022.xlsx** : a second dataset on invasive plant growth for the different treatments and the 3 sites. This file contains the metrics (Height, No. of leaves, No. of branches, Number, Number of nodes).
Due to spatial autocorrelation, the surveys of the 3 1m2 quadrats were merged into 1 single survey. Thus all the data of this workbook are summarized in a single sheet with the following description:
![](images/null_Resume.png)

###### <div align="center">Description of the IAS evolution dataset</div>



- **Rejetinvasivesbilan2021-2022.xlsx** : a second set of data on the growth of invasive plants for the different treatments and the 3 sites. This file contains the metrics (Height, Nb leaves, Nb branches, Number of plants, Number of nodes).


- **Mesure Plantes Allélopathiques 2022.xlsx** : contains the measure of allelopathic plants.

## 2. Work Completed

### 2.1. Data processing

#### REEVES dataset

In order to create machine learning algorithms, we had to prepare the data.

The data we had at our disposal were data in Excel format, so we tried to transform them into CSV format data.

Due to the lack of information in some datasets, we decided to keep only the datasets with the most information. So we decided to summarize all the sheets of the dataset data.xlsx in the file dataset.xlsx. Thanks to this data formatting, we realized that we were missing a lot of information (see the red boxes). We wondered if we could simulate the missing data by framing them between two known values for example. Unfortunately, as we only had 2 surveys (one in 2021 and one in 2022), we could not predict the missing data by framing. We therefore concluded that the data.xlsx file could not be used in machine learning models because the results would be completely wrong due to the lack of data present in it.

We therefore decided to focus on the file Rejetinvasivesbilan2021-2022.xlsx because it was the one that was most likely to be used. Once the data in this file was converted to CSV format, in order to integrate it into a decision tree algorithm, we had to find a way to express the different treatments in terms of binary values. For each measure, we created a column indicating 1 if the treatment corresponding to the column is present on this measure and 0 otherwise.

We also calculated the variation of the different measurements made for each plot over time by identifying them according to their treatment and their site.

### 2.2. Data visualization

The objective of this part is to carry out the visualization of data, in particular to try to bring out patterns in our data and the evolution of the IAS. This visualization has been realized with the help of python libraries: Matplotlib and Seaborn.

N.B.: In this chapter we will only present the most relevant results obtained from the data contained in the file Rejetinvasivesbilan2021-2022. However, we have provided other notebooks containing other less relevant graphs.

**Correlation between the data**

![](images/Correlation_res.png)

**Evolution of the average height of IAS per treatment**

![](images/Hauteur.png)
![](images/Evolution_hauteur.png)

**Interpretation: change in height**.
We can see that the average height is 160 cm for the control plots and 175 cm for the invasives.
The above graphs show that all treatments with allelopatic species slow down the evolution of IAS in terms of height. But it is the treatments **NA1 with compost, and NA2** that are the most efficient with a slowing down of more than **50%**.

#### Evolution of the total number of leaves per treatment

![](images/total_nbfeuille.png)

![](images/evolution_nbf.png)

#### Interpretation

First of all, the measurements taken in November 2021 cannot be considered very significant in terms of treatment efficiency because it is a winter period and the plants die off by themselves in winter.

However, these graphs show us a great dimunition of the foliar density (number of leaves) of the IAS for some treatments:

- for the treatments **NA1 with compost**, **Single compost** and **NA2**, we have a decrease of the leaf density of the IAS of approximately 50%.
- On the other hand, the decrease is less noticeable for the treatments **NA2 with compost**, **Single compost** and **NA1**.

## 3. Machine learning model

### 3.1. Description of the algorithms used
#### &nbsp;&nbsp;&nbsp;&nbsp; a. PCA

 Principal Component Analysis (PCA) is an unsupervised learning algorithm that allows to reduce the dimension (number of variables) of the data without losing much information and thus facilitating their visualization. In addition, PCA also increases the performance of machine learning algorithms because it is less efficient on high dimensional data.

 The idea is to transform correlated variables into new decorrelated variables by projecting the data in the direction of increasing variance. The variables with the maximum variance will be chosen as the main components.

#### &nbsp;&nbsp;&nbsp;&nbsp; b. Kmeans

 The Kmeans algorithm is an iterative algorithm that attempts to partition a data set into K distinct subgroups or clusters, where each data point belongs to a single group.
 More generally, the k-means algorithm follows the following steps:

1. Choose k points, by a random draw without remittance and consider them as centroids
2. Distribute the points in the k classes thus formed according to their proximity to the centroids
3. Use the barycenters of the classes as new centroids and repeat until there are no more changes.

Note: This algorithm has a strong dependence on the choice of the number k of classes

***Elbow method***

The elbow method allows us to choose a good number k of clusters. It is based on the sum of the squared distance (SSD) between the data points and the centroids of their clusters predicted by the algorithm. So we just plot the SSD against the K and we select k at the point where SSE starts to flatten and form a elbow.

#### &nbsp;&nbsp;&nbsp;&nbsp; c. Decision Tree

Approach in "If ... Then ...". The objective is to divide the values we have, from several parameters. Thus, we are able to solve regression or classification problems.
The basic idea is to divide the data into smaller and smaller subgroups according to specific characteristics until each subgroup can no longer be divided.
To separate our data, we need to think about how we do it, and how we choose the decision values:
The idea is to find the most important variable to classify our data. This is done by calculating the importance of each variable. The more important the variable is, the earlier it is used in the tree.

- The algorithm searches for the most important variable for the classification of the data.
- Then, the algorithm will divide the data according to the most important criterion, to make 2 subgroups.
- Then we reproduce these steps by recurrence, until all our data are classified in a homogeneous way.

#### &nbsp;&nbsp;&nbsp;&nbsp; d. Random forest

The principle of the random forest is simple, it is to multiply the trees. It is an algorithm based on the assembly of decision trees.
A random forest is made of a set of independent decision trees.
Each tree has a fragmented view of the problem because of a double random draw: a random draw with replacement on the observations and a random draw on the variables.
At the end, all these independent decision trees are assembled. And the final estimation consists in choosing the most frequent answer category.


### 3.2. Implementation and interpretation of the results

Because of the low frequency of measurement and the numerous gaps in our dataset, it is impossible to consider a regression task on our data. We have therefore decided to classify the treatments that have been carried out on the EEE in order to try to rank them from the least effective to the most effective.

In order to apply the decision trees, we need to separate our data into inputs and outputs.

On the input side, we take the characteristics of the applied treatment:
$ X =( [\text {NA1, NA1+Soil, NA2, NA2+Soil, Soil, Temoin, Invasive} ]*[\text{with compost, without compost}])^n$ with n the number of record lines

and in output the evolution of the IAS (variation of the number, the height and the number of leaves).

Y =( text {var. Number, var. Height, var. No. Leaves})$

The current state of our data does not allow us to exploit them directly with a decision tree, so we will follow the steps below:

- **Formatting the inputs** :

 First we will transform our inputs into binary inputs such as; $ X =( \text {NA1,NA2, Soil, Template, Invasive, Compost} ) \text{with NA1, NA2, Soil, Temoin, Invasive, Compost }\epsilon [0,1]^n $(1 if the treatment in the line contains the corresponding treatment and 0 otherwise )

Example : In the capture below, the treatment corresponding to line 0 is (NA1, without compost) and the one corresponding to line 6 is (NA2, without compost)

![](images/example.png)

If there are 1's on NA1, Soil and Compost at the same time, this corresponds to the treatment (NA1+Soil, With compost).

- **Transformation of the output**:

Here, the objective is to gather the three output variables into a single variable which will represent the monotony of the evolution of the IAS (For example 3 groups: Growth(C), Stable(S), Decrease(D)).

The different graphs realized do not show any pattern on the data. We will therefore perform a clustering of our data using the Kmeans algorithm.

First of all, following a study of the correlation between our output variables, we notice that the correlations are a bit high, which motivates our choice to perform a PCA on our data in order to increase the performance of our K-means algorithm.

![](images/Correlation_sortie.png)

- PCA :
After normalizing our data, we apply PCA to obtain their projections on the main axes.

![](images/ACP.png)

We notice that the explained variance of the first two components PC1 and PC2 represents more than **80%** of the total explained variance. So PC1 and PC2 contain more than 80% of the information contained in all our output variables; it is with them that we will divide our data into clusters.

- K-means :

The first step is to determine the optimal value of K (Number of centroids). We use **the elbow method** as follows:

![](images/coude.png)

We observe a kink for **K=3**.

We thus carry out our clustering with k=3.

![](images/Kmeans.png)

###### <div align="center">K-means Algorithm</div>

The graph below represents the different clusters.

![](images/kmeans_graphic.png)

###### <div align="center">Output data cluster</div>

<div align="justify">Text or image</div>

This result coincides with the hypothesis of 3 monotonies in the evolution of IAS. However, at first sight, we do not know what each cluster corresponds to. We will therefore interpolate our clusters with our initial data.

![](images/data_cluster.png)

Next, we plot the scatterplots of our output variables against our clusters:

![](images/graphe_cluster.png)

#### Interpretation

- class 0 : corresponds to a stabilization of the evolution because the point cloud is centered in 0.
- class 1 : Increase of the IAS, the height and the number of leaves are the most important.
- class 2 : Decrease of the IAS, Nf < 0 and stable height.

#### Decision Tree

We are now going to apply the decision tree on our data, with as input: $X =( \text {NA1,NA2, Soil, Temoin, Invasive, Compost} ) \text{with NA1, NA2, Soil, Temoin, Invasive, Compost }\epsilon [0,1]^n $
and in output: $Y =(cluster)$

![](images/decision_tree.png)

###### <div align="center">Decision Tree</div>

We therefore obtain an accuracy score of **60.5%**.

We then make predictions about our treatments:

![](images/Prediction.png)

![](images/result_pred.png)

## 4. Second dataset

During our project, we turned to other datasets because the ones provided to us lacked sufficient information to make use of reliable results. We decided to look at an alternative dataset on the impact of exotic plants on native plants in forestry operations and vice versa. The idea was to see, from this dataset, if forest exploitation had an impact on the invasion of invasive plants and if the combination of these invasive plants had an impact on the local plants. To do this we had access to 3 datasets:

- PlotDataBiotropica.csv,which contains the complete database of characteristics of each of the 159 vegetation plots, including forest exploitation measures, environmental variables, and specific data on native and exotic plants.
- SpeciesDataBiotropica.csv, which contains the complete list of species (723) sampled in 192 vegetation plots (2 x 2 m) located within the SAFE (Stability of Altered Forest Ecosystems) project in Sabah, Malaysia, including a distinction between the origin of native and alien species.
- PlotSpeciesDataBiotropica, which contains a matrix of biomass values for 723 plant taxa sampled in 192 vegetation plots (2 x 2 m) located within the Stability of Altered Forest Ecosystems (SAFE) project in Sabah, Malaysia.

The dataset we retrieved was complete, so we tried to visualize the data using Kepler. The only problem was that the coordinates were in UTM format which is not a format that can be used in Kepler, so we used an API that allows us to convert the coordinates into longitude/latitude coordinates and then add the resulting coordinates to the dataset.
The conversion file of these data is available with the path (alternatives_data/UTM_to_lon_lat.ipynb).

***Data visualization with Kepler***

The Kepler tool allows us to see where the plots in the dataset are located geographically. We can also add a color contrast on each of the displayed points to have a quick overview of the variation of the measurement we are looking at from one point to another.
an example of visualization is available in the notebook (/alternatives_data/visualization_geographic.ipynb).


## 5. Problems encountered

During the course of this project, we encountered some difficulties that forced us to do some project review on several occasions.

First, there were many missing values in the dataset we retrieved and it was not possible to try to find them because too few measurements were made.

The lack of data preventing us from performing a correct machine learning algorithm led us to search for a new dataset.

After a week of searching, we had to retain a new dataset on a similar project. However, we could not retain this dataset to do machine learning on it because it was a static dataset, which was against the problematic of the topic. However, we were able to do some geographic visualization on it.

Finally, we decided to go back to the initial dataset even though we knew that it would not allow us to have exploitable results.


## 6. Proposal for improvement

 We find that the accuracy of our decision tree is at most 60%, which is not sufficient to clearly identify a treatment as effective. To improve the chances of success, we could
  
- Take measurements at more regular intervals, which would give us a larger data set. This would also allow us to apply regression models to predict future outcomes.

- In terms of the types of data collected, we used data corresponding to the number of invasive plants, their height, and their number of leaves.
One of the datasets contains information on the proportion of invasive species in a plot. It would be possible to link the datasets to get additional information, to link the datasets more easily.
We can give identifiers to the plots, so that if we make different measurements on one of them, it is possible to identify them in all the datasets so that they can be linked.

- In order to identify the most effective treatment, it will be necessary to be able to define on which criteria a treatment is considered effective on the invasive species (-> For example, if its proportion decreases by more than 50%, then the treatment can be considered effective).


- It can also be interesting to take into account the meteorological conditions, the soil moisture and the sunshine during the measurements. During our research, we found that it was possible to consult the weather data, but that to have a detailed history, a subscription was necessary. So we stopped it here, but it is possible to couple this data to the previous ones to be able to take into account the environment. In the alternative dataset we looked at, the data included soil pH parameters, which can also influence the behavior of plant species.

## Conclusion

This project allowed us to understand the work of a data analyst. First of all, we were able to learn about an interesting project that could help in a consequent way the maintenance of the railroad tracks. It was a real opportunity for us to participate in this concrete project, although our results were not very conclusive.
We were able to work on the data processing which was a big part of the work. We were also able to learn about machine learning and experiment with some algorithms. We were also able to apply our project management skills throughout the project, thanks to numerous meetings to review our progress.
This whole project allowed us to realize the importance of data and the possibilities offered by machine learning. However, we were disappointed that we were not able to deliver reliable results.
We hope that in the next few years, the data will be more extensive which will eventually lead to a satisfactory result.