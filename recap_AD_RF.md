# Récapitulatif sur les Arbres de Décision et Random Forest :

## Arbres de Decision :

Approche en mode "Si ... Alors ...". L'objectif est de diviser les valeurs que l'on possède, à partir de plusieurs paramètres. Ainsi, on est capable de résoudre des problème de régression ou de classification.
L'idée de base est de diviser les données en sous-groupes de plus en plus petits en fonction de caractéristiques spécifiques jusqu'à ce que chaque sous-groupe ne puisse plus être divisé.

Pour séparer nos données, il faut réfléchir à comment on le fait, et comment on choisit les valeurs de décision :

L'idée est de trouver la variable la plus importante pour classifier nos données. Cela se fait en calculant l'importance de chaque variable. Plus la variable est importante, plus elle est utilisée tôt dans l'arbre.

- cherche la variable la plus importante pour la classification des données.
- ensuite, l'algo va diviser les données selon le critère le plus important, pour en faire 2 sous-groupes.
- on reproduit ensuite par récurrence ces étapes, jusqu'à ce que toutes nos données soient classifiées de manière homogène.

Les variables pourront par exemple être :
* le site de la plante
* le type de plante
* le traitement qui est utilisé + compost ou pas
* le nombre de feuilles au moment du relevé de données (a voir si on couple au nombre de plantes (PCA))
* la proportion de plante invasive
* etc...

### Attention!!
Il faudra bien penser à diviser au préalable notre jeu de données pour pouvoir avoir un jeu d'entrainement du modèle, et un jeu de test et de validation (validation croisée).

Avant de coder, il faudra donc voir quels sont les paramètres que l'on utilise, et dans quel ordre on les utilise (l'ordre pourra être modifié en cours de route, pour voir et constater que notre modèle est bel et bien fonctionnel et optimisé)

## Random Forest
